#version 450

layout (location = 0) in vec3 pos;
layout (location = 1) in vec3 col;

layout (location = 0) uniform mat4 proj;
layout (location = 1) uniform mat4 view;
layout (location = 2) uniform mat4 world;

out vec4 vcol;

void main() {
    gl_Position = proj * view * world * vec4(pos, 1);
    vcol = vec4(col, 1);
}