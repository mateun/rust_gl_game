#version 450

in vec4 vcol;

out vec4 color;

void main() {
    color = vcol;
}