

pub struct VirtualMachine {
    mem : [u8; 1024],   
}

impl VirtualMachine {

    pub fn new() -> VirtualMachine {

        let mem_array : [u8; 1024] = [0;1024];
        Self {mem : mem_array}
    }

    pub fn mem_get(&self, address:usize) -> u8 {
        self.mem[address]
    }

    pub fn mem_set(&mut self, address:usize, val:u8)  {
        let mut m = &mut self.mem;
        m[address] = val;
    }
}