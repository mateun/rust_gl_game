
extern crate nalgebra_glm as glm;
mod gl_util;
mod file_utils;
mod vm;
use  gl_util::{window::{GLWindow, WindowEvent}, 
        shader::{Shader, ShaderProgram },
        vertex::{VertexBuffer, VertexArray, IndexBuffer},
        render::{RenderCommand, MatrixTransforms}, 
        math::{Vec3, Vec4}};
use vm::VirtualMachine;

struct GameState {
    pub score : i32,
    pub position : glm::Vec3,
    pub angle : f32,
    pub vao : VertexArray,
    pub shader_prog : ShaderProgram,
}

impl GameState {
    pub fn inc_pos(&mut self) {
        self.position += glm::vec3(0.001, 0.0, 0.0);
    }

    pub fn inc_angle(&mut self, val :f32) {
        self.angle += val;
    }
}

fn check_gl_error() -> Result<u8, String>{
    let err = unsafe {gl::GetError() };
    if err != 0 {
       return Err(format!("glError: {}", err));
    } else {
        Ok(0)
    }
}

fn math_tests() {
    let v1 = Vec3::v3(1., 0., 0.);
    let v2 = Vec3::v3(0., 0., -1.);
    let vc = v1.cross(&v2);
    assert_eq!(vc.x, 0.);
    assert_eq!(vc.y, 1.);
    assert_eq!(vc.z, 0.);
    let vc2 = v2.cross(&v1);
    assert_eq!(vc2.y, -1.);
}

fn update(frame_time : f32, game_state:&mut GameState) {

    game_state.inc_pos();
    game_state.inc_angle(0.1 * frame_time * 400.);
}

fn render(game_state:&GameState) {
    let eye = &glm::vec3(0., 0., 10.);
    let center = glm::vec3(0., 0.1, 5.);
    let up = glm::vec3(0., 1.,0.);
    let mp = glm::perspective(800./600., 0.2 , 0.1, 200.);
    let mv = glm::look_at(&eye, &center, &up);
    let mw = glm::translation(&game_state.position);
    let angle = glm::radians(&glm::vec1(game_state.angle))[0];
    let mr = glm::rotation(angle ,&glm::vec3::<f32>(-0., 1., 0.));
    let mats = MatrixTransforms::new(mp, mv, mw * mr, 0, 1, 2);
    let rc1 = RenderCommand::new(mats);
    rc1.execute(&game_state.shader_prog, &game_state.vao);
}

fn main() {
    let win = GLWindow::new(800, 600);

    let vs = &Shader::new_from_file("assets/vs.glsl", gl::VERTEX_SHADER).unwrap();
    let fs = &Shader::new_from_file("assets/fs.glsl", gl::FRAGMENT_SHADER).unwrap();
    let prog = ShaderProgram::from_shaders(vs, fs).unwrap();
    let perf_freq = win.perf_freq();

    println!("perf Freq: {}", perf_freq);
    let pos = vec! [
        -0.5, -0.5, 0.,
        0.5, -0.5, 0.,
        0.5, 0.5, 0., 
        -0.5, 0.5, 0.
    ];

    let vcol = vec! [
        1., 0., 0.,
        0., 1., 0.,
        0., 0., 1.0, 
        1., 1., 1.
    ];

    let indices = vec![
        0, 1, 2,
        0, 2, 3
    ];




    let vbtri = VertexBuffer::from_data(pos, 0, 3, gl::FLOAT);
    let vbcol = VertexBuffer::from_data(vcol, 1, 3, gl::FLOAT);
    let vbind = IndexBuffer::from_data(indices );
    let mut gl_err = check_gl_error().unwrap();
    let vaotri = VertexArray::new_with_buffers(vec![vbtri, vbcol], vbind);
    gl_err = check_gl_error().unwrap();

    

    math_tests();

    let vm = VirtualMachine::new();

    let mut game_state = GameState { score: 0, position: glm::Vec3::new(-1., 0., -3.), angle: 0., vao: vaotri, shader_prog: prog};
    
    let mut start_time = win.perf_tick();
    'main:loop {
        
        let mut e: WindowEvent;
        let mut none_found = false;
        while (!none_found) {
            e = win.poll_event();
            match e {
                WindowEvent::Quit=> break 'main,
                WindowEvent::None => none_found = true, 
                _=> (),
            }
        } 

        win.clear();
        let end_time = win.perf_tick();
        let tick_diff = end_time - start_time;
        let frame_time :f32 = ((tick_diff as f32 / (win.perf_freq()) as f32) as f32) ;
        start_time = win.perf_tick();
        println!("frametime: {}", frame_time);
        update(frame_time, &mut game_state);
        render(&game_state);
        
        gl_err = check_gl_error().unwrap();
        win.swap();
        

    }
    

}
