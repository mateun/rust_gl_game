extern crate sdl2;
extern crate gl;

use std::cell::RefCell;



pub struct GLWindow {
    _sdl : sdl2::Sdl,
    window : sdl2::video::Window,
    ep_cell : RefCell<sdl2::EventPump>,
    _gl_context : sdl2::video::GLContext,
    perf_freq : u64,
}
pub enum WindowEvent {
    None,
    Quit,
}

impl GLWindow {
    pub fn new(w : u32, h : u32) -> GLWindow {
        let sdl = sdl2::init().unwrap();
        let video_subsystem = sdl.video().unwrap();
        let gl_attr = video_subsystem.gl_attr();
        gl_attr.set_context_profile(sdl2::video::GLProfile::Core);
        gl_attr.set_context_version(4, 5);
    
        let window = video_subsystem
                        .window("Game", w, h)
                        .opengl()
                        .resizable()
                        .build()
                        .unwrap();
        let gl_context = window.gl_create_context().unwrap();
        let _gl = gl::load_with(|s| video_subsystem.gl_get_proc_address(s) as *const std::os::raw::c_void);

        unsafe {
            gl::Viewport(0, 0, w.try_into().unwrap(), h.try_into().unwrap());
            gl::ClearColor(0.3, 0.2, 0.2, 1.0);
        }

        let event_pump = sdl.event_pump().unwrap();
        let epc : RefCell<sdl2::EventPump> = RefCell::new(event_pump);
        let perf_freq = sdl.timer().unwrap().performance_frequency();
    
        Self {_sdl:sdl, window, ep_cell: epc, _gl_context: gl_context, perf_freq}
    }

    pub fn perf_tick(&self) -> u64 {
        self._sdl.timer().unwrap().performance_counter()
    }

    pub fn perf_freq(&self) -> u64 {
        self.perf_freq
    }

    pub fn poll_event(&self) -> WindowEvent {
        let e = self.ep_cell.borrow_mut().poll_event();
        if e.is_some() {
            return match e.unwrap() {
                sdl2::event::Event::Quit { .. } => WindowEvent::Quit,
                _ => WindowEvent::None,
            }
        } else {
            return WindowEvent::None;
        }
        
        
    }

    pub fn clear(&self)  {
        unsafe {
            gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);
        }
    }

    pub fn swap(&self) {
        self.window.gl_swap_window();
    }
}

