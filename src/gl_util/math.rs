use std::num;

pub struct Mat4 {

}

pub struct Vec4 {
    pub x :f32,
    pub y :f32,
    pub z:f32,
    pub w:f32,
}

impl Vec4 {
    pub fn v4(x: f32, y:f32, z:f32, w:f32) -> Vec4 {
        Self {x, y, z, w}
    }

    pub fn dot(&self, other: &Vec4) -> f32 {
      (self.x * other.x + self.y * other.y + self.z * other.z + self.w * other.w)
    }

    pub fn normalize(&self) -> Vec4 {
        let lensqared = self.x * self.x + self.y * self.y + self.z * self.z + self.w * self.w;
        let len = (lensqared as f32).sqrt();
        Vec4::v4(self.x / len, self.y / len, self.z / len, self.w / len)
    }

    pub fn add(&self, other:&Vec4) -> Vec4 {
        Vec4::v4(self.x + other.x, self.y + other.y, self.z + other.z, self.w + other.w)
    }
    
    pub fn subtract(&self, other:&Vec4) -> Vec4 {
        Vec4::v4(self.x - other.x, self.y - other.y, self.z - other.z, self.w - other.w)
    }


        
}

pub struct Vec3 {
    pub x :f32,
    pub y :f32,
    pub z :f32
}

impl Vec3 {

    pub fn v3(x: f32, y:f32, z:f32) -> Vec3 {
        Self {x, y, z}
    }

    pub fn dot(&self, other: &Vec3) -> f32 {
      (self.x * other.x + self.y * other.y + self.z * other.z)
    }

    pub fn normalize(&self) -> Vec3 {
        let lensqared = self.x * self.x + self.y * self.y + self.z * self.z;
        let len = (lensqared as f32).sqrt();
        Vec3::v3(self.x / len, self.y / len, self.z / len)
    }

    pub fn cross(&self, other:&Vec3) -> Vec3 {
        Vec3::v3 (self.y * other.z - self.z * other.y, self.z*other.x - self.x * other.z, self.x*other.y - self.y * other.x)
    }
        
   
}

impl std::ops::Add<Vec3> for Vec3 {
    type Output = Vec3;

    fn add(self, other : Vec3) -> Vec3{
        let result :Vec3 = Vec3::v3 (self.x + other.x, self.y + other.y, self.z + other.z);
        result
    }
}

impl std::ops::Sub<Vec3> for Vec3 {
    type Output = Vec3;

    fn sub(self, other: Vec3) -> Vec3 {
        Vec3::v3 (self.x - other.x, self.y - other.y, self.z - other.z)
    }
}

