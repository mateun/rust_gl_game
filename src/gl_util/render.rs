extern crate gl;
extern crate nalgebra_glm as glm;
use crate::gl_util::vertex::*;
use crate::gl_util::shader::*;
use gl::types::*;

pub struct MatrixTransforms {
    mat_proj : glm::TMat4<f32>,
    mat_view : glm::TMat4<f32>,
    mat_world : glm::TMat4<f32>,
    proj_slot : i32,
    view_slot : i32,
    world_slot : i32,
}

pub struct RenderCommand {
    matrix_transforms: MatrixTransforms,
}

impl MatrixTransforms {

    pub fn new(proj:glm::TMat4<f32>, view:glm::TMat4<f32>, world:glm::TMat4<f32>, 
        proj_slot : i32, view_slot:i32, world_slot:i32) -> MatrixTransforms {
            Self {mat_proj : proj, mat_view: view, mat_world: world, proj_slot, view_slot, world_slot}
    }

    pub fn proj_slot(&self) -> i32 {
        self.proj_slot
    }

    pub fn view_slot(&self) -> i32 {
        self.view_slot
    }

    pub fn world_slot(&self) -> i32 {
        self.world_slot
    }

    pub fn proj(&self) -> *const f32{
        self.mat_proj.data.as_slice().as_ptr()
    }

    pub fn view(&self) -> *const f32 {
        self.mat_view.data.as_slice().as_ptr()
    }

    pub fn world(&self) -> *const f32 {
        self.mat_world.data.as_slice().as_ptr()
    }

}

impl RenderCommand {

    pub fn new(mat_transforms : MatrixTransforms) 
        -> RenderCommand {
        Self {matrix_transforms : mat_transforms}
    }

    pub fn execute(&self, shader_prog:&ShaderProgram, vao:&VertexArray) {

        shader_prog.bind();
        unsafe {
            gl::UniformMatrix4fv(self.matrix_transforms.view_slot(), 1, gl::FALSE, self.matrix_transforms.view() as *const GLfloat);
            gl::UniformMatrix4fv(self.matrix_transforms.proj_slot(), 1, gl::FALSE, self.matrix_transforms.proj() as *const GLfloat);
            gl::UniformMatrix4fv(self.matrix_transforms.world_slot(), 1, gl::FALSE, self.matrix_transforms.world() as *const GLfloat);
        }

        vao.bind();
        vao.render();

    }
    
}