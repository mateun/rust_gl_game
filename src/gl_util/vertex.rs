extern crate gl;
use gl::types::*;


pub struct VertexBuffer {
    id : GLuint,
    data : Vec<f32>,
    shader_slot : u16,
    number_of_components : u8,
    gl_type : GLenum,

}

pub struct IndexBuffer {
    id: GLuint, 
    data : Vec<u32>,

}

impl IndexBuffer {
    pub fn from_data(data:Vec<u32>) -> IndexBuffer {

        let mut id : GLuint = 0;
        let size = data.len() * std::mem::size_of::<u32>();
        unsafe {
            gl::GenBuffers(1, &mut id);
            gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, id);
            gl::BufferData(gl::ELEMENT_ARRAY_BUFFER, 
                size as GLsizeiptr, 
                data.as_ptr() as *const GLvoid, 
                gl::STATIC_DRAW);
        }
        Self {id, data}
    }

    pub fn id(&self) -> GLuint {
        self.id
    }
}

pub struct VertexArray {
    id: GLuint,
    vertex_buffers : Vec<VertexBuffer>,
    index_buffer : IndexBuffer
}

impl VertexBuffer {
    pub fn from_data(data:Vec<f32>, slot: u16, num_components: u8, gl_type : GLenum) -> VertexBuffer {
        let mut vbo : GLuint = 0;
        let size = data.len() * std::mem::size_of::<f32>();
        unsafe { 
            gl::GenBuffers(1, &mut vbo);
            gl::BindBuffer(gl::ARRAY_BUFFER, vbo);
            gl::BufferData(
                gl::ARRAY_BUFFER, 
                size as GLsizeiptr,
                data.as_ptr() as *const GLvoid,
                gl::STATIC_DRAW
            );
            gl::BindBuffer(gl::ARRAY_BUFFER, 0);
        }

        Self { id:vbo, data, shader_slot: slot, 
            number_of_components:num_components, gl_type }
    }

    

    pub fn id(&self) -> GLuint {
        self.id
    }

    pub fn slot(&self) -> u16 {
        self.shader_slot
    }

    pub fn number_of_components(&self) -> usize {  
        self.number_of_components.try_into().unwrap() 
    }
    pub fn gl_type(&self) -> GLenum {
        self.gl_type
    }
}

impl VertexArray {
    pub fn new_with_buffers(vertex_buffers: Vec<VertexBuffer>, index_buffer: IndexBuffer) -> VertexArray {

        let mut vao : GLuint = 0;
        unsafe { 
            gl::GenVertexArrays(1, &mut vao);
            gl::BindVertexArray(vao);
            gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, index_buffer.id());
            
            for buffer in vertex_buffers.iter()  {
            
                    gl::BindBuffer(gl::ARRAY_BUFFER, buffer.id());
                    gl::VertexAttribPointer(
                        buffer.slot().try_into().unwrap(),
                        buffer.number_of_components().try_into().unwrap(),
                        buffer.gl_type(),
                        gl::FALSE,
                        0,
                        std::ptr::null()
                    );
                    gl::EnableVertexAttribArray(buffer.slot().try_into().unwrap());
        
                    
            
            }

            gl::BindVertexArray(0);

        }
        
        Self {id :vao, vertex_buffers, index_buffer }

    }

    pub fn bind(&self) {
        unsafe {
            gl::BindVertexArray(self.id);
        }
    }

    pub fn ib_len(&self) -> i32 {
        self.index_buffer.data.len().try_into().unwrap()
    }

    pub fn render(&self) {
        unsafe {
            gl::DrawElements(gl::TRIANGLES, self.ib_len(), gl::UNSIGNED_INT, std::ptr::null());
        }
        
    }
}

