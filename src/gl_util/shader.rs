extern crate gl;
use std::fs;
use crate::file_utils;
use gl::types::*;

pub struct ShaderProgram {
    prog_id : GLuint,
}


pub struct Shader {
    id : GLuint,
}

impl ShaderProgram {
    pub fn from_shaders(vs:&Shader, fs:&Shader) -> Result<ShaderProgram, String> {
        let id = unsafe { gl::CreateProgram() }; 
        unsafe {
            gl::AttachShader(id, vs.id());
            gl::AttachShader(id, fs.id());
            gl::LinkProgram(id);
            let mut success : gl::types::GLint = 1;
            unsafe {
                gl::GetProgramiv(id, gl::LINK_STATUS, &mut success);
            }
            if success == gl::FALSE.into() {
                println!("Could not link shader prog!");
                let err_msg = Shader::get_log(id);
                println!("shader error: {}", err_msg);
                return Err(format!("error linking program!"));
            }

            
        }

        let p = Self {prog_id:id};
        unsafe {
            gl::DetachShader(id, vs.id());
            gl::DetachShader(id, fs.id());
            gl::DeleteShader(vs.id());
            gl::DeleteShader(fs.id());
        }

        Ok(p)
    }

    pub fn id(&self) -> GLuint {
        self.prog_id
    }

    pub fn bind(&self) {
        unsafe {
            gl::UseProgram(self.prog_id);
        }
    }
}

impl Shader {

    pub fn id(&self) -> gl::types::GLuint {
        self.id
    }

    pub fn new_from_file(file_name:&str, kind: gl::types::GLuint) 
    -> Result<Shader, String> {
        let mut path = std::env::current_dir().unwrap();
        let path_r = format!("{}/../../", path.display());

        println!("current dir: {}", path_r);
        let full_path = format!("{}/{}", path_r, file_name);
        println!("full path: {}", full_path);
        let code = fs::read_to_string(full_path).unwrap();
        let shader_id = Shader::new_from_source(&code, kind).unwrap();

        Ok(Self{id:shader_id})
    }

    pub fn new_from_source(source:&str, kind: gl::types::GLuint) 
    ->  Result<gl::types::GLuint, String>
    {
        let id = unsafe { gl::CreateShader(kind)};
        let source_cstr = std::ffi::CString::new(source).unwrap();    
    
        unsafe {
            gl::ShaderSource(id, 1, &source_cstr.as_ptr(), std::ptr::null());
            gl::CompileShader(id);
    
            let mut success : gl::types::GLint = 1;
            unsafe {
                gl::GetShaderiv(id, gl::COMPILE_STATUS, &mut success);
            }
    
            if success == 0 {
                return Err(Shader::get_log(id));
            }
        }
    
        Ok(id)
    }

    fn get_log(id:GLuint) -> String {
        let mut len : gl::types::GLint = 0;
            unsafe {
                gl::GetShaderiv(id, gl::INFO_LOG_LENGTH, &mut len);
            }
            let mut buffer:Vec<u8> = Vec::with_capacity(len as usize + 1);
            buffer.extend([b' '].iter().cycle().take(len as usize));
            let error = file_utils::create_cstring_with_len(len as usize);
            unsafe {
                gl::GetShaderInfoLog(id, len, 
                    std::ptr::null_mut(),
                error.as_ptr() as *mut gl::types::GLchar);
            }
            return error.to_string_lossy().into_owned();
    }
}